using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Yong.TestFoo
{
    public class CameraFollow : MonoBehaviour
    {
        [SerializeField]
        Transform followPoint;

        private void LateUpdate()
        {
            transform.position = followPoint.position;
            transform.rotation = followPoint.rotation;
        }
    }

}
