using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Yong.TestFoo
{
    public class AnimeTest : MonoBehaviour
    {
        Animator anim;
        private void Awake()
        {
            anim = GetComponent<Animator>();
        }

        private void Update()
        {
        }

        public void UpdateAnimation(float speed)
        {
            anim.SetFloat("Speed", speed);
        }

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(transform.position + Vector3.up, (transform.position + transform.forward + Vector3.up));
        }
    }

}
