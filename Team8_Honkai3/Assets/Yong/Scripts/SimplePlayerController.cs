using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Yong.TestFoo
{
    public class SimplePlayerController : MonoBehaviour
    {
        [SerializeField]
        float moveSpeed = 6f;
        [SerializeField]
        Transform cameraFollowPoint;
        public UnityEvent<float> OnWalking;
        private void Awake()
        {
        }

        void Move()
        {
            var v = Input.GetAxis("Vertical");
            var h = Input.GetAxis("Horizontal");
            var move = Vector3.zero;
            if(v != 0f)
            {
                move = transform.forward * v;
            }
            if(h != 0f)
            {
                move += transform.right * h;
            }

            OnWalking.Invoke(move.magnitude);
            move = move * moveSpeed * Time.deltaTime;
            transform.position += move;
        }
        void UpdateCamera()
        {
            var horizontal = Input.GetAxis("Mouse X");
            var vertical = Input.GetAxis("Mouse Y");
            vertical = Mathf.Clamp(vertical, -80f, 45f);
            cameraFollowPoint.Rotate(new Vector3(0f,horizontal,0f));
        }
        private void Update()
        {
            Move();
        }
    }
}
