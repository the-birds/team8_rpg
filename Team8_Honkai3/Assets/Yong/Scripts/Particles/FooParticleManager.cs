using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FooParticleManager : MonoBehaviour
{
    ParticleSystem[] particles;
    private void Awake()
    {
        enabled = false;
    }
    private void Start()
    {
        particles = FindObjectsOfType<ParticleSystem>();
    }

}
