using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonController : MonoBehaviour
{
    [SerializeField]
    private Transform mainCamera;

    private CharacterController controller;

    [SerializeField]
    private float speed;

    [SerializeField]
    private float turnSmoothTime;

    private float turnSmoothVelocity;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        float horizontalValue = Input.GetAxisRaw("Horizontal");
        float verticalValue = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontalValue, 0.0f, verticalValue).normalized;

        if (direction.magnitude < 0.01f)
        {
            return;
        }

        float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + mainCamera.localRotation.eulerAngles.y;
        float angle = Mathf.SmoothDampAngle(transform.localRotation.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);

        transform.localRotation = Quaternion.Euler(0f, angle, 0f);

        Vector3 moveDirection = Quaternion.Euler(0.0f, targetAngle, 0.0f) * Vector3.forward;
        controller.Move(moveDirection.normalized * speed * Time.fixedDeltaTime);

    }
}
